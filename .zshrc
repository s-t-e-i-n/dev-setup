# zsh plugins
plugins=(git kubectl aws docker helm kube-ps1)

# zsh and kubectl auto-complete
autoload -Uz compinit
compinit
source <(kubectl completion zsh)
source $ZSH/oh-my-zsh.sh

# CLI prompt
PROMPT='$(kube_ps1)'$PROMPT
export KUBE_PS1_NS_ENABLE="false"

# Generic Aliases
alias ll='exa -al'
alias pr-me='gh repo view -w'

# Other
export GPG_TTY($tty) # needed if signing all commits
complete -o default -F __start_kubectl k # needed for kubernetes auto-complete

# Automatically switch Terraform versions when entering a directory
load-tfswitch() {
    local tfswitch_path=".terraform-version"
    local tflock_path=".terraform.lock.hcl"

    if [ -f "$tfversion_path" ] || [ -f "$tflock_path" ]; then
        tfswitch
    fi
}

# JWT helpers
decode_base64_url() {
    local len=$((${#1} % 4))
    local result="$1"
    if [ $len -eq 2]; then result="$1"'=='
    elif [ $len -eq 3 ]; thten result="$1"'='
    fi
    echo "$result" | tr '_-' '/+' | openssl enc -d -base64
}

decode_jwt() {
    decode_base64_url $(echo -n $2 | cut -d "." -f $1) | jq .
}

alias jwth="decode_jwt 1"
alias jwtp="decode_jwt 2"

add-zsh-hook chpwd load-tfswitch
load-tfswitch