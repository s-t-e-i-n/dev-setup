# dev-setup

## Tooling

### Install xcode tools

```zsh
xcode-select --install
```

### Install Homebrew

```zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

brew analytics off
brew bundle
```

### VSCode

User Settings
```json
"files.trimTrailingWhitespace": true,
"files.insertFinalNewLine": true,
"files.trimFinalNewLines": true,
```

Plugins
- Terraform
- Python
- Markdown All in One
- Markdown reflow

### ZSH

[Install oh-my-zsh](https://onmyz.sh)